<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Notif extends MX_Controller {

	function __construct(){
		parent::__construct();
		$sess = $this->session->userdata('logged_in');
		$this->theme = $sess['section_name'];
	}

	function webadmin(){
				$path='assets/themes/'.$this->theme.'/notif/webadmin.php';
				if(file_exists($path)){	
					$this->viewPath = '../../assets/themes/'.$this->theme.'/notif/';
					$this->load->view($this->viewPath.'webadmin');
				} else {
					$this->load->view('notif/webadmin');
				}
	}
}
?>