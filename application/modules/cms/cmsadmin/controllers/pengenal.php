<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Pengenal extends MX_Controller {

	function __construct(){
		parent::__construct();
		$sess = $this->session->userdata('logged_in');
		$this->nama_user = $sess['nama_user'];
		$this->theme = $sess['section_name'];
	}

	function webadmin(){
		$data['nama'] = $this->nama_user;

				$path='assets/themes/'.$this->theme.'/pengenal/webadmin.php';
				if(file_exists($path)){	
					$this->viewPath = '../../assets/themes/'.$this->theme.'/pengenal/';
					$this->load->view($this->viewPath.'webadmin',$data);
				} else {
					$this->load->view('pengenal/webadmin',$data);
				}
	}
}
?>